import 'style.scss'
import ReactDOM from 'react-dom'
import React, { Component } from 'react'
import PropTypes from 'prop-types'

const Screen = ({ displayText, onLimpiar }) => (
  <div className="screen">
    <button type="button" onClick={onLimpiar}>
      x
    </button>
    {displayText}
  </div>
)

Screen.propTypes = {
  displayText: PropTypes.string.isRequired,
  onLimpiar: PropTypes.func.isRequired,
}

const Button = ({ value, ...props }) => (
  <button type="button" className="btn" {...props}>
    {value}
  </button>
)

Button.propTypes = {
  value: PropTypes.string.isRequired,
}

class Application extends Component {
  state = {
    operador: '',
    variableA: '',
    variableB: '',
    resultado: 0,
    buttons: [
      ['1', '2', '3', '+'],
      ['4', '5', '6', '-'],
      ['7', '8', '9', '/'],
      ['0', '.', '=', 'x'],
    ],
  }

  onLimpiar = () => {
    this.setState({
      operador: '',
      variableA: '',
      variableB: '',
      resultado: 0,
    })
  }

  handleClick = (e) => {
    const value = e.target.innerText

    switch (value) {
      case '+':
      case '-':
      case '*':
      case '/': {
        this.setState({
          operador: value,
        })

        break
      }
      case '=': {
        let resultado = 0
        const { variableA, variableB, operador } = this.state

        if (operador === '+') {
          resultado = parseFloat(variableA) + parseFloat(variableB)
        } else if (operador === '-') {
          resultado = parseFloat(variableA) - parseFloat(variableB)
        } else if (operador === '*') {
          resultado = parseFloat(variableA) * parseFloat(variableB)
        } else {
          resultado = parseFloat(variableA) / parseFloat(variableB)
        }

        this.setState({
          resultado,
          operador: '',
          variableA: resultado,
          variableB: '',
        })

        break
      }
      default: {
        const { operador } = this.state

        if (operador !== '') {
          this.setState(prevState => ({
            variableB: prevState.variableB + value,
          }))
        } else {
          this.setState(prevState => ({
            variableA: prevState.variableA + value,
          }))
        }
        break
      }
    }
  }

  render() {
    const {
      buttons, variableA, variableB, operador, resultado,
    } = this.state

    return (
      <>
        <Screen
          displayText={resultado || (operador ? variableB : variableA)}
          onLimpiar={this.onLimpiar}
        />
        {buttons.map((arr, index) => (
          <div key={index}>
            {arr.map(value => <Button key={value} value={value} onClick={this.handleClick} />)}
            <br />
          </div>
        ))}
      </>
    )
  }
}

ReactDOM.render(<Application />, document.getElementById('app'))
