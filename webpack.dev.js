import merge from 'webpack-merge' // eslint-disable-line
import path from 'path'

import common from './webpack.common'

export default merge(common, {
  devServer: {
    contentBase: path.join(__dirname, 'src'),
    historyApiFallback: true,
  },
  devtool: 'eval',
  mode: 'development',
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, 'src'),
    publicPath: '/',
  },
})
