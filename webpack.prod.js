import merge from 'webpack-merge' // eslint-disable-line
import path from 'path'

import common from './webpack.common'

export default merge(common, {
  mode: 'production',
  output: {
    filename: '[chunkhash].[name].js',
    path: path.join(__dirname, 'build'),
    publicPath: '/',
  },
})
